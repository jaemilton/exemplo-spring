package br.com.itau.marketplace.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.itau.marketplace.models.Cliente;
import br.com.itau.marketplace.repositories.ClienteRepository;

@Service
public class ClienteService {
	@Autowired
	ClienteRepository clienteRepository;
	
	public Iterable<Cliente> obterClientes(){
		return clienteRepository.findAll();
	}
	
	public Optional<Cliente> buscarClientePorId(int id){
		return clienteRepository.findById(id);
	}
	
	public void inserirCliente(Cliente cliente) {
		clienteRepository.save(cliente);
	}
}
