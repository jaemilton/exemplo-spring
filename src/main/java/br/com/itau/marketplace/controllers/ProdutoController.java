package br.com.itau.marketplace.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.itau.marketplace.models.Produto;
import br.com.itau.marketplace.repositories.ProdutoRepository;

@RestController
@RequestMapping("/produto")
public class ProdutoController {

	@Autowired
	ProdutoRepository produtoRepository;
	
	@GetMapping
	public Iterable<Produto> listar() {
		return produtoRepository.findAll();
	}
	
	@PostMapping
	public void inserir(@RequestBody Produto produto) {
		produtoRepository.save(produto);
	}
}
