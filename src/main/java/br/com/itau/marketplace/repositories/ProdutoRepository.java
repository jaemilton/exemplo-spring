package br.com.itau.marketplace.repositories;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import br.com.itau.marketplace.models.Produto;

public interface ProdutoRepository extends CrudRepository<Produto, Integer> {

	public Optional<Produto> findByNome(String nome);
}
